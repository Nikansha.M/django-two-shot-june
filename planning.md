### Set Up Directory and GitLab Project

1. Cd into your projects directory
1. Fork and Clone the starter project to your Projects Directory:
    1. Since I already have a Django-One-Shot repository in GitLab, additional steps I need to  follow:
        1. Create a blank GitLab project, set visibility to Public, initialize **without** the README file
        1. While in your projects directory:  git clone https://gitlab.com/sjp19-public-resources/django-two-shot.git (SJP)
        1. Cd django-one-shot
        1. Git remote rm origin (to remove the origin branch)
        1. Git remote add origin https://gitlab.com/Nikansha.M/django-two-shot-june.git (clone the blank Django-One-Shot project I created in my GitLab repository - the one without the readme file
        1. Git remote -v
            1. This will list out:
                1. origin  https://gitlab.com/Nikansha.M/django-two-shot-june.git (fetch)
                1. origin  https://gitlab.com/Nikansha.M/django-two-shot-june.git (push)
        1. git push --set-upstream origin main (this is a one-time command you’ll use)
1. Refresh GitLab page, you should now see the same commits that were made to the original SJP project on GitLab

TO PUSH YOUR PROJECT UP TO GITLAB, USE **git push origin main**


### Set Up Commands

1. Create Virtual environment:  **python -m venv .venv**
1. Activate Virtual environment:  **./.venv/Scripts/Activate.ps1**
1. Upgrade pip:  **python -m pip install --upgrade pip**
1. Install django:  **pip install django**
1. Install black:  **pip install black**
1. Install flake8:  **python -m pip install flake8**
1. Install djhtml:  **pip install djhtml**
1. Install djlint:  **pip install djlint**
1. Deactivate your virtual environment:  **deactivate**
1. Activate your virtual environment:  **./.venv/Scripts/Activate.ps1**
1. Use pip freeze to generate a requirements.txt file:  **pip freeze > requirements.txt**


#### Create Django Project - expenses

1. Create the django PROJECT named expenses:  
    1. **django-admin startproject brain_two .** (don’t forget the dot!!!)
1. Run migrations:
    1. python manage.py makemigrations
    1. python manage.py migrate
1. Create a superuser:
    1. python manage.py createsuperuser


#### Create Django Apps - accounts & receipts
1. Create a django app named accounts and one named receipts:  
    1. python manage.py startapp accounts
    1. python manage.py startapp receipts
1. settings.py -- Under Installed_Apps, install both apps in the EXPENSES Django project
    1. "accounts.apps.AccountsConfig",
    1. "receipts.apps.ReceiptsConfig",


#### Create the Models in the *receipts* django app & register them in Receipts.Admin
1. ExpenseCategory Model
1. Account Model - is the way that we paid for it, such as with a specific credit card or a bank account.
1. Receipt Model - primary thing that this application keeps track of for accounting purposes.


#### Create ReceiptListView
1. in receipts/views.py create ReceiptListView
    1. this will get all of the instances of the Receipt Model (from receipts/models.py) and puts them in context for the template
1. create receipts/urls.py and create the app path for the ListView
1. In expenses/urls.py include url patterns from receipts app
1. create base.html and home.html templates
1. in expenses/urls.py use the RedirectView to redirect from "" to home ()


#### Accounts App LoginView
1. in accounts/urls.py register LoginView ✔️
1. register the accounts app in the URL patterns in the expenses/urls.py file ✔️
1. create templates directory under accounts -> create registration directory -> create login.html template in registration directory ✔️
    1. create a post form: ✔️
    <form method="post">
        {% csrf_token %}
        {{ form.as_p }}
        Login button goes here
    </form>
1. add LOGIN_REDIRECT_URL = "home" to the bottom of the expenses/settings.py file ✔️


#### Accounts App LogoutView
1. in accounts/urls.py:
    1. import LogoutView (same as LoginView) and register the LogoutView in urlpatterns list ✔️
1. in expenses/settings.py create and set variable LOGOUT_REDIRECT_URL = "login" (to redirect logout view to login page) ✔️


#### Filtering Receipts
1. Protect ListView for Receipt model so only a person who is logged in can access it (LoginRequiredMixin) ✔️
1. Change queryset of the view to filter Receipt objects where purchaser = logged in user ✔️


#### Signup
1. in accounts -> templates -> registration create signup.html
    1. use a form = "POST"
1. In accounts/views.py create the function: def signup(request) 
    1. import -- from django.contrib.auth.forms import UserCreationForm 
    1. use create_user() method to create a new user
    1. import and use login function
    1. after user has been created, redirect browser to home
1. in accounts/urls.py add signup path to urlpatterns


#### Add Login, Logout, and Signup links to base.html
1. If user is signed in (or authenticated): logout link ✔️
1. if user is not signed in: signup or login link ✔️


#### Create ReceiptCreateView
1. Receipt CreateView with fields: vendor, total, tax, date, category and account
    1. person must be logged in (LoginRequiredMixin)
1. Register that view for the path "create/" in the receipts urls.py and the name "create_receipt" 
1. create the create.html 
